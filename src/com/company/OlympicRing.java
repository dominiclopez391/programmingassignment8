package com.company;

import java.awt.*;
import java.awt.geom.Ellipse2D;

/*
 * Dominic Lopez
 * Programming Assignment 7
 * CS49J
 * */

public class OlympicRing {

    int x, y, r;
    Color color;

    public OlympicRing(int x, int y, int r, Color color) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.color = color;
    }

    public void draw(Graphics2D g2) {
        Ellipse2D.Double ellipse = new Ellipse2D.Double(x, y, r, r);
        g2.setColor(color);
        g2.draw(ellipse);
    }

}
