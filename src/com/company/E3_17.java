package com.company;

import javax.swing.*;

/*
 * Dominic Lopez
 * Programming Assignment 7
 * CS49J
 * */

public class E3_17 { //EllipseViewer

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(1280, 800);
        frame.setResizable(true);
        frame.setTitle("Colored Ellipse");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(new EllipseComponent());

        frame.setVisible(true);
    }
}
