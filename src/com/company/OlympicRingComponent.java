package com.company;

import javax.swing.*;
import java.awt.*;

/*
 * Dominic Lopez
 * Programming Assignment 7
 * CS49J
 * */

public class OlympicRingComponent extends JComponent {

    public OlympicRing[] rings;
    final float width = 6f;

    public OlympicRingComponent(int x, int y, int r) {

        rings = new OlympicRing[5];
        rings[0] = new OlympicRing(x, y, r, Color.BLUE);
        rings[1] = new OlympicRing(x + r + (int)width, y, r, Color.BLACK);
        rings[2] = new OlympicRing(x + 2*r + (int)width * 2, y, r, Color.RED);
        rings[3] = new OlympicRing(x + r/2, y + r / 2, r, Color.YELLOW);
        rings[4] = new OlympicRing(x + 3*r/2 + (int)width, y + r / 2, r, Color.GREEN);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(width));

        for(int i = 0; i < rings.length; i++) {
            rings[i].draw(g2);
        }


    }
}
