package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

/*
 * Dominic Lopez
 * Programming Assignment 7
 * CS49J
 * */

public class EllipseComponent extends JComponent {

    @Override
    public void paintComponent(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;
        Ellipse2D.Double ellipse = new Ellipse2D.Double(0, 0, getWidth(), getHeight());
        g2.setColor(new Color(192, 255, 238));
        g2.fill(ellipse);


        ellipse = new Ellipse2D.Double(25, 25, getWidth() - 50, getHeight() - 50);
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke(50));
        g2.draw(ellipse);


    }

}
