package com.company;

import javax.swing.*;

/*
* Dominic Lopez
* Programming Assignment 7
* CS49J
* */

public class E3_24 { //OlympicRingViewer

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(1280, 800);
        frame.setTitle("Olympic Rings");
        frame.setResizable(true);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new OlympicRingComponent(30, 30, 150));
        frame.setVisible(true);


    }
}
